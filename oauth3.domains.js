;(function (exports) {
'use strict';

var OAUTH3 = exports.OAUTH3 = exports.OAUTH3 || require('./oauth3.core.js').OAUTH3;

OAUTH3.api['domains.checkAvailability'] = function (providerUri, opts) {
  var session = opts.session;
  var sld = opts.sld;
  var tld = opts.tld;

  return OAUTH3.request({
    method: 'GET'
  , url: OAUTH3.url.normalize(providerUri)
      + '/api/com.daplie.domains/check-availability/' + sld + '/' + tld
  , session: session
  }).then(function (res) {
    return res;
  });
};

OAUTH3.api['domains.purchase'] = function (providerUri, opts) {
  var session = opts.session;

  return OAUTH3.request({
    method: 'POST'
  , url: OAUTH3.url.normalize(providerUri)
      + '/api/com.daplie.domains/accounts/' + session.token.sub + '/registrations'
  , session: session
  , data: opts.data
  }).then(function (res) {
    return res;
  });
};

OAUTH3.api['domains.list'] = function (providerUri, opts) {
  var session = opts.session;

  return OAUTH3.request({
    method: 'GET'
  , url: OAUTH3.url.normalize(providerUri)
      + '/api/com.daplie.domains/accounts/' + session.token.sub + '/registrations'
  , session: session
  }).then(function (res) {
    return res.data.registrations || res.data.domains || res.data;
  });
};

// TODO: Manual Renew Function
OAUTH3.api['domains.extend'] = function (providerUri, opts) {
  var session = opts.session;

  return OAUTH3.request({
    method: 'POST'
  , url: OAUTH3.url.normalize(providerUri)
      + '/api/com.daplie.domains/accounts/' + session.token.sub + '/registrations/' + opts.data.tld + '/' + opts.data.sld + '/extend'
  , session: session
  , data: opts.data
  }).then(function (res) {
    return res;
  });
};


OAUTH3.api['ns.list'] = function (providerUri, opts) {
  var session = opts.session;
  var domain = opts.domain;
  var nameArr = domain.split('.');
  var reverseNameArr = nameArr.reverse();
  var nameSubArr = reverseNameArr.slice(3);
  var tld;
  var sld;
  var sub;

  if (reverseNameArr[0] === 'me' && reverseNameArr[1] === 'daplie') {
    tld = 'daplie.me';
    sld = reverseNameArr[2];
    sub = nameSubArr.reverse().join('.') || '';
  } else {
    tld = nameArr[0];
    sld = nameArr[1];
    sub = reverseNameArr.slice(2).reverse().join('.') || '';
  }

  return OAUTH3.request({
    method: 'GET'
  , url: OAUTH3.url.normalize(providerUri)
      + '/api/com.daplie.domains/accounts/' + session.token.sub + '/ns/'
      + tld + '/' + sld + '/' + sub
  , session: session
  }).then(function (res) {
    return res.data;
  });
};

OAUTH3.api['ns.add'] = function (providerUri, opts) {
  var session = opts.session;
  var server = opts.server;
  var tld =  opts.tld;
  var sld = opts.sld;
  var sub = opts.sub;

  return OAUTH3.request({
    method: 'POST'
  , url: OAUTH3.url.normalize(providerUri)
      + '/api/com.daplie.domains/accounts/' + session.token.sub + '/ns/'
      + tld + '/' + sld + '/' + sub
  , session: session
  , data: { nameservers: [server] }
  }).then(function (res) {
    return res;
  });
};

OAUTH3.api['glue.list'] = function (providerUri, opts) {
  var session = opts.session;

  return OAUTH3.request({
    method: 'GET'
  , url: OAUTH3.url.normalize(providerUri)
      + '/api/com.daplie.domains/accounts/' + session.token.sub + '/glue'
  , session: session
  }).then(function (res) {
    return res.data;
  });
};

OAUTH3.api['glue.add'] = function (providerUri, opts) {
  var session = opts.session;
  var ip = opts.ip;
  var tld =  opts.tld;
  var sld = opts.sld;
  var sub = (opts.sub || '@');

  return OAUTH3.request({
    method: 'POST'
  , url: OAUTH3.url.normalize(providerUri)
      + '/api/com.daplie.domains/accounts/' + session.token.sub + '/glue/'
      + tld + '/' + sld + '/' + sub
  , session: session
  , data: { ip: ip }
  }, {}).then(function (res) {
    return res;
  });
};

}('undefined' !== typeof exports ? exports : window));
