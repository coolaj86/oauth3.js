;(function () {
'use strict';

var modules = {
  azp: [
    '$timeout'
  , '$q'
  , '$rootScope'
  , function Oauth3($timeout, $q, $rootScope) {
      var OAUTH3 = window.OAUTH3;

      // We need to make angular's $q appear to be a standard Promise/A+
      // fortunately, this is pretty easy
      function PromiseAngularQ(fn) {
        var d = $q.defer();

        //$timeout(function () {
          fn(d.resolve, d.reject);
        //}, 0);

        //this.then = d.promise.then;
        //this.catch = d.promise.catch;
        return d.promise;
      }

      //PromiseAngularQ.create = PromiseAngularQ;
      PromiseAngularQ.resolve = $q.when;
      PromiseAngularQ.reject = $q.reject;
      PromiseAngularQ.all = $q.all;

      OAUTH3.PromiseA = PromiseAngularQ;
      OAUTH3._digest = function () {
        $rootScope.$digest();
      };

      window.ngOauth3 = OAUTH3;

      return OAUTH3;
    }
  ]
};

angular
  .module('oauth3.org', [])
  .service('azp@oauth3.org', modules.azp)
  .service('AzpOauth3', modules.azp)
  ;
}());
