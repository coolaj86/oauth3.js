;(function () {
  'use strict';

  var gulp = require('gulp');
  var browserify = require('browserify');
  var source = require('vinyl-source-stream');
  var streamify = require('gulp-streamify');
  var uglify = require('gulp-uglify');
  var rename = require('gulp-rename');

  gulp.task('default', function () {
    return browserify('./browserify/crypto.fallback.js', {standalone: 'OAUTH3_crypto_fallback'}).bundle()
      .pipe(source('browserify/crypto.fallback.js'))
      .pipe(rename('oauth3.crypto.fallback.js'))
      .pipe(gulp.dest('./'))
      .pipe(streamify(uglify()))
      .pipe(rename('oauth3.crypto.fallback.min.js'))
      .pipe(gulp.dest('./'))
      ;
  });

  gulp.task('watch', function () {
    gulp.watch('browserify/*.js', [ 'default' ]);
  });
}());
