;(function (exports) {
'use strict';

var OAUTH3 = exports.OAUTH3 = exports.OAUTH3 || require('./oauth3.core.js').OAUTH3;

OAUTH3.api['dns.list'] = function (providerUri, opts) {
  var session = opts.session;

  return OAUTH3.request({
    method: 'GET'
  , url: OAUTH3.url.normalize(providerUri)
      + '/api/com.daplie.domains/accounts/' + session.token.sub + '/dns'
  , session: session
  }).then(function (res) {
    return res.data.records || res.data;
  });
};

OAUTH3.api['devices.list'] = function (providerUri, opts) {
  var session = opts.session;

  return OAUTH3.request({
    url: OAUTH3.url.normalize(providerUri)
      + '/api/com.daplie.domains/accounts/' + session.token.sub + '/devices'
  , method: 'GET'
  , session: session
  }, {}).then(function (res) {
    return res.data.devices || res.data;
  });
};

OAUTH3.api['devices.attach'] = function (providerUri, opts) {
  var session = opts.session;
  var device = opts.device;
  var tld = opts.tld;
  var sld = opts.sld;
  var sub = opts.sub;
  var ip = opts.ip;
  var ttl = opts.ttl;

  return OAUTH3.request({
    url: OAUTH3.url.normalize(providerUri)
      + '/api/com.daplie.domains/accounts/' + session.token.sub + '/devices/'
      + device + '/' + tld + '/' + sld + '/' + (sub || '')
    , method: 'POST'
    , session: session
    , data: {
        addresses: ip
      , ttl: ttl
    }
  }, {}).then(function (res) {
    return res.data.devices || res.data;
  });
};

OAUTH3.api['devices.detach'] = function (providerUri, opts) {
  var session = opts.session;
  var device = opts.device;
  var tld = opts.tld;
  var sld = opts.sld;
  var sub = opts.sub;

  return OAUTH3.request({
    url: OAUTH3.url.normalize(providerUri)
      + '/api/com.daplie.domains/accounts/' + session.token.sub
			+ '/devices/' + device + '/' + tld + '/' + sld + '/' + (sub || '')
  , method: 'DELETE'
  , session: session
  }, {}).then(function (res) {
    return res.data.device || res.data;
  });
};

OAUTH3.api['devices.destroy'] = function (providerUri, opts) {
  var session = opts.session;
  var device = opts.device;

  return OAUTH3.request({
    url: OAUTH3.url.normalize(providerUri)
      + '/api/com.daplie.domains/accounts/' + session.token.sub
			+ '/devices/' + device
  , method: 'DELETE'
  , session: session
  }, {}).then(function (res) {
    return res.data.device || res.data;
  });
};

OAUTH3.api['dns.set'] = function (providerUri, opts) {
  var session = opts.session;
  var tld = opts.tld;
  var sld = opts.sld;
  var sub = opts.sub;
  var type = opts.type;
  var value = opts.value;
  var ttl = opts.ttl;
  var priority = (opts.priority || '');
  var weight = (opts.weight || '');
  var port = (opts.port || '');

  return OAUTH3.request({
    url: OAUTH3.url.normalize(providerUri)
      + '/api/com.daplie.domains/accounts/' + session.token.sub
      + '/dns/' + tld + '/' + sld + '/' + sub
  , method: 'POST'
  , session: session
  , data: [{
    type: type
  , value: value
  , ttl: ttl
  , priority: priority
  , weight: weight
  , port: port
  }]
  }, {}).then(function (res) {
    return res.data || res;
  });
};

OAUTH3.api['dns.unset'] = function (providerUri, opts) {
  var session = opts.session;
  var tld = opts.tld;
  var sld = opts.sld;
  var sub = (opts.sub || '@');
  var type = opts.type;
  var value = opts.value;

  return OAUTH3.request({
    url: OAUTH3.url.normalize(providerUri)
      + '/api/com.daplie.domains/accounts/' + session.token.sub
      + '/dns/' + tld + '/' + sld + '/' + sub + '/' + type + '/' + value
  , method: 'DELETE'
  , session: session
  }, {}).then(function (res) {
    return res.data || res;
  });
};

}('undefined' !== typeof exports ? exports : window));
