;(function (exports) {
'use strict';

var OAUTH3 = exports.OAUTH3 = exports.OAUTH3 || require('./oauth3.core.js').OAUTH3;

OAUTH3.api['tunnel.token'] = function (providerUri, opts) {
  var session = opts.session;

  return OAUTH3.request({
    method: 'POST'
  , url: OAUTH3.url.normalize(providerUri)
      + '/api/tunnel@oauth3.org/accounts/' + session.token.sub + '/token'
  , session: session
  , data: {
      domains: opts.data.domains
    , device: opts.data.device
    }
  }).then(function (res) {
    return res.data.records || res.data;
  });
};

}('undefined' !== typeof exports ? exports : window));
