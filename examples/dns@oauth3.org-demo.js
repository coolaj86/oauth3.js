oauth3.api('devices.list').then(function (result) {
  console.log(result);
});

oauth3.api(
  'devices.set'
, { data: {
    name: 'tester.local'
  , uid: 'test-01-uid'
  , addresses: [
      {
        type: 'A'
      , address: '192.168.1.104'
      }
    ]
  } }
).then(function (result) {
  console.log('devices.set');
  console.log(result);
});

// TODO don't allow attaching if the device is not set
// TODO update API as well
oauth3.api(
  'devices.attach'
, { data: {
    sub: 'test-01'
  , sld: 'aj'
  , tld: 'daplie.me'
  , uid: 'test-01-uid'
  } }
).then(function (result) {
  console.log('devices.attach');
  console.log(result);
});

oauth3.api(
  'devices.detach'
, { data: {
    sub: 'test-01'
  , sld: 'aj'
  , tld: 'daplie.me'
  , uid: 'test-01-uid'
  } }
).then(function (result) {
  console.log('devices.detach');
  console.log(result);
});
