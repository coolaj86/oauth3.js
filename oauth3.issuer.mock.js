;(function (exports) {
'use strict';

  var OAUTH3 = exports.OAUTH3 = exports.OAUTH3 || require('./oauth3.core.js').OAUTH3;

  OAUTH3.authn.resourceOwnerPassword = OAUTH3.authz.resourceOwnerPassword = function (directive, opts) {
    var providerUri = directive.issuer;

    if (opts.mockError) {
      return OAUTH3.PromiseA.resolve({data: {error_description: "fake error", error: "errorcode", error_uri: "https://blah"}});
    }

    return OAUTH3._mockToken(providerUri, opts);
  };
  OAUTH3.authn.loginMeta = function (directive, opts) {
    if (opts.mockError) {
      return OAUTH3.PromiseA.resolve({ data: { error: { message: "Yikes!", code: 'E' } } });
    }
    return OAUTH3.PromiseA.resolve({ data: {} });
  };
  OAUTH3.authn.otp = function (directive, opts) {
    if (opts.mockError) {
      return OAUTH3.PromiseA.resolve({data: {error: {message: "Yikes!", code: 'E'}}});
    }
    return OAUTH3.PromiseA.resolve({data: {uuid: "uuidblah"}});
  };

  OAUTH3.authz.scopes = function () {
    return OAUTH3.PromiseA.resolve({
      pending: [ 'authn@oauth3.org' ]     // not yet accepted
    , granted: []                         // all granted, ever
    , requested: [ 'authn@oauth3.org' ]   // all requested, now
    , accepted: []                        // granted (ever) and requested (now)
    });
  };
  OAUTH3.authz.grants = function (providerUri, opts) {
    if ('POST' === opts.method) {
      return OAUTH3._mockToken(providerUri, opts);
    }

    return OAUTH3.discover(providerUri, {
      client_id: providerUri
    , debug: opts.debug
    }).then(function (directive) {
      return {
        client: {
          name: "foo"
        , client_id: "localhost.foo.daplie.me:8443"
        , url: "https://localhost.foo.daplie.me:8443"
        }
      , grants: []
      };
    });
  };

  OAUTH3._refreshToken = function (providerUri, opts) {
    return OAUTH3._mockToken(providerUri, opts);
  };

  OAUTH3._mockToken = function (providerUri, opts) {
    var payload = { exp: Math.round(Date.now() / 1000) + 900, sub: 'fakeUserId', scp: opts.scope };
    return OAUTH3.crypto._signPayload(payload).then(function (accessToken) {
      return OAUTH3.hooks.session.refresh(
        opts.session || {
          provider_uri: providerUri
        , client_id: opts.client_id
        , client_uri: opts.client_uri || opts.clientUri
        }
      , { access_token: accessToken
        , refresh_token: accessToken
        , expires_in: "900"
        , scope: opts.scope
        }
      );
    });
  };

}('undefined' !== typeof exports ? exports : window));
