(function () {
'use strict';

function create(myOpts) {
  return {
    requestScope: function (opts) {
      // TODO pre-generate URL

      // deliver existing session if it exists
      var scope = opts && opts.scope || [];
      if (myOpts.session) {
        if (!scope.length || scope.every(function (scp) {
          return -1 !== opts.myOpts.session.scope.indexOf(scp);
        })) {
          return OAUTH3.PromiseA.resolve(myOpts.session);
        }
      }

      // request a new session otherwise
      return OAUTH3.implicitGrant(myOpts.directives, {
        client_id: myOpts.conf.client_uri
      , client_uri: myOpts.conf.client_uri
        // maybe use inline instead?
      , windowType: 'popup'
      , scope: scope
      }).then(function (session) {
        return session;
      });
    }
  , session: function () {
      return myOpts.session;
    }
  , refresh: function (session) {
      return OAUTH3.implicitGrant(myOpts.directives, {
        client_id: myOpts.conf.client_uri
      , client_uri: myOpts.conf.client_uri
      , windowType: 'background'
      }).then(function (_session) {
        session = _session;
        return session;
      });
    }
  , logout: function () {
      return OAUTH3.logout(myOpts.directives, {
        client_id: myOpts.conf.client_uri
      , client_uri: myOpts.conf.client_uri
      });
    }
  , switchUser: function () {
      // should open dialog with user selection dialog
    }
  }
}

window.navigator.auth = {
  getUserAuthenticator: function (opts) {
    var conf = {};
    var directives;
    var session;

    opts = opts || {};
    conf.client_uri = opts.client_uri || OAUTH3.clientUri(opts.location || window.location);

    return OAUTH3.issuer({ broker: opts.issuer_uri || 'https://new.oauth3.org' }).then(function (issuer) {
      conf.issuer_uri = issuer;
      conf.provider_uri = issuer;

      return OAUTH3.directives(conf.provider_uri, {
        client_id: conf.client_uri
      , client_uri: conf.client_uri
      }).then(function (_directives) {
        directives = _directives;
        var myOpts = {
          directives: directives
        , conf: conf
        };

        return OAUTH3.implicitGrant(directives, {
          client_id: conf.client_uri
        , client_uri: conf.client_uri
        , windowType: 'background'
        }).then(function (_session) {
          session = _session;
          myOpts.session = session;
          return create(myOpts);
        }, function (err) {
          console.error('[DEBUG] implicitGrant err:');
          console.error(err);
          return create(myOpts);
        });
      });
    });
  }
};

}());
