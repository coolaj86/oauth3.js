;(function (exports) {
'use strict';

var OAUTH3 = exports.OAUTH3 = exports.OAUTH3 || require('./oauth3.core.js').OAUTH3;

OAUTH3.api['account.listCards'] = function (providerUri, opts) {
  var session = opts.session;

  return OAUTH3.request({
    method: 'GET'
  , url: OAUTH3.url.normalize(providerUri)
      + '/api/com.daplie.payments/accounts/' + session.token.sub + '/cards'
  , session: session
  }).then(function (res) {
    return res;
  });
};

OAUTH3.api['account.addCard'] = function (providerUri, opts) {
  var session = opts.session;

  return OAUTH3.request({
    method: 'POST'
  , url: OAUTH3.url.normalize(providerUri)
      + '/api/com.daplie.payments/accounts/' + session.token.sub + '/cards'
  , session: session
  , data: opts.data
  }).then(function (res) {
    return res;
  });
};

OAUTH3.api['account.removeCard'] = function (providerUri, opts) {
  var session = opts.session;

  return OAUTH3.request({
    method: 'DELETE'
  , url: OAUTH3.url.normalize(providerUri)
      + '/api/com.daplie.payments/accounts/' + session.token.sub + '/cards/' + opts.last4 + '/' + opts.brand
  , session: session
  }).then(function (res) {
    return res;
  });
};

OAUTH3.api['account.listAddresses'] = function (providerUri, opts) {
  var session = opts.session;

  return OAUTH3.request({
    method: 'GET'
  , url: OAUTH3.url.normalize(providerUri)
      + '/api/com.daplie.me/accounts/' + session.token.sub + '/addresses'
  , session: session
  }).then(function (res) {
    return res;
  });
};

OAUTH3.api['account.addAddress'] = function (providerUri, opts) {
  var session = opts.session;

  return OAUTH3.request({
    method: 'POST'
  , url: OAUTH3.url.normalize(providerUri)
      + '/api/com.daplie.me/accounts/' + session.token.sub + '/addresses'
  , session: session
  , data: opts.addAddress
  }).then(function (res) {
    return res;
  });
};

OAUTH3.api['account.removeAddress'] = function (providerUri, opts) {
  var session = opts.session;

  return OAUTH3.request({
    method: 'DELETE'
  , url: OAUTH3.url.normalize(providerUri)
      + '/api/com.daplie.me/accounts/' + session.token.sub + '/addresses/' + opts.addressId
  , session: session
  }).then(function (res) {
    return res;
  });
};

}('undefined' !== typeof exports ? exports : window));
